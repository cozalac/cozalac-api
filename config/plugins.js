module.exports = ({ env }) => ({
  // ...
  email: {
    provider: 'sendgrid',
    providerOptions: {
      apiKey: env('SENDGRID_API_KEY'),
    },
    settings: {
      defaultFrom: 'contact@cozalac.com',
      defaultReplyTo: 'contact@cozalac.com',
    },
  },
  // ...
});
