module.exports = ({ env }) => ({
  host: env('HOST'),
  port: env.int('PORT'),
  admin: {
    url: env('ADMIN_PATH'),
    auth: {
      secret: env('ADMIN_JWT_SECRET'),
    },
  },
});
